R package: APRLmpf
===

APRL (multi-)peak fitting tool. Resolves overlapping bands according to procedure and values described by Takahama, Johnson, and Russell, *Aerosol Sci. Tech.*, 2013.

More generally, the purpose of this package is to provide a framework for

* locating peak positions
* simultaneously fitting peaks and fixed profile shapes using different search algorithms and multiple conditions for fitting (e.g., initial values)
* visualizing fits
* converting fitted peaks to [micro]moles of a substance

Subject to substantial modification. Keep up-to-date with `git pull`.

## General Instructions

First, please see [installation instructions for APRLspec](https://gitlab.com/aprl/APRLspec), on which APRLmpf depends.

Install for R (replace `{username}` and `{password}` with account for which access to private repository is granted):

```{r}
> devtools::install_git('https://{username}:{password}@gitlab.com/aprl/APRLmpf.git')
```

After installation, get a copy of example files (templates). To copy them into a new folder called "mpf" in your working directory:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLmpf', 'example_files', 'mpf')"
$ cd mpf/
```
To copy them into an existing working directory ("." as third argument) - e.g., the current one where you have used baseline correction scripts - you can optionally pass a prefix as the fourth argument to the `CopyPkgFiles` function:

```{sh}
$ cd /path/to/workingdir/
$ R -e "APRLspec::CopyPkgFiles('APRLmpf', 'example_files', '.', 'mpf')"
```
This will add a prefix to all the example files in this directory to prevent naming conflicts with files from other packages.

To uninstall, run the command in R:

```{sh}
> remove.packages("APRLmpf")
```

## Fit peaks

```
$ Rscript main_fits.R testcase/userinput.json
```

Key parameters in .json file:

* `specfile`: sectra file
* `peaksequence`: list of peaks or profiles to be fitted in sequence
* `limitcCOH`: limit estimated moles[carboxylic COH] to be less than or equal to estimated moles[carboxylic CO]. carboxylic CO must appear before carboxylic COH in `peaksequence`

Outputs:

* `fitpars.csv`
* `pkareas.csv`

## Plot fits

```
$ Rscript plot_fits.R testcase/userinput.json
```
Key parameters in .json file:

* `specfile`: sectra file

Outputs:

* `specfits.pdf`

## Apply Beer-Lambert law


```
$ Rscript conv_areas.R testcase/userinput.json
```

Key parameters in .json file:

* `abscoef`: (optional) json file of absorption coefficients

Outputs:

* `moles.csv`: micromoles per collection surface area

## Test cases

* testcase: Use with main\_fits.R. Most similar to Takahama et al. (2013).
* testcase2: Use with main\_fits.R. Experimental -- fit everything together.
* testcase3: Use with main\_fits\_constr\_cCOH.R. Constrain moles of carboxylic COH <= moles of carboyl CO.

## Future work

* improve I/O
* allow specification of numerical maskbounds
* Lorentzian and Voigt lineshapes
